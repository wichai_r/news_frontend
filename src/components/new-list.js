import React, { Component } from 'react';
import axios from 'axios';
import { Config } from '../config';

const NewItem = props => (
    <div style={{marginBottom: 10 ,borderWidth: 2}}>
        <a href={'/new/'+props.item.id}>
            <div style={{display:'inline-block' ,justifyContent: 'center', minWidth: 200 }}>
                <img src={props.item.image} 
                    style={{ 
                        display: 'block',
                        marginLeft: 'auto',
                        marginRight: 'auto',
                        maxHeight: 150 , 
                        maxWidth: 150
                    }}
                alt={props.item.title} />
            </div>
            <div style={{display:'inline-block' }}>
                <div style={{ fontWeight: 'bold'}}>{props.item.title} ( { props.item.created_date } )</div>
                <div>{props.item.description}</div>
            </div>
        </a>
    </div>
)

export default class NewList extends Component {

    constructor(props){
        super(props);
        this.state = {
            newList : []
        };
    }

    componentDidMount() {
        axios.get(Config.apiUrl + 'newlist')
            .then(response => {
                
                if(response.data.result){
                    this.setState({newList : response.data.newList});
                }
            })
            .catch( (error) =>{

            })
    }

    renderNewList() {
        return this.state.newList.map(function(currentItem, i){
            return <NewItem item={currentItem} key={i} />;
        })
    }

    render() {

        return (
            <div>
                { this.renderNewList() }
            </div>
        )
    }
}