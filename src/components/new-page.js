import React, { Component } from 'react';
import axios from 'axios';
import { Config } from '../config';

export default class NewPage extends Component {

    constructor(props){
        super(props);
        this.state = {
            new : {}
        };
    }

    componentDidMount() {
        
        axios.get(Config.apiUrl + 'new/'+this.props.match.params.id)
            .then(response => {
                
                if(response.data.result){
                    this.setState({new : response.data.new});
                }
            })
            .catch( (error) =>{

            })
    }

    render() {

        return (
            <div>
                <div>
                    <img src={this.state.new.image} 
                        style={{ 
                            display: 'block',
                            marginLeft: 'auto',
                            marginRight: 'auto',
                            maxHeight: 150 , 
                            maxWidth: 150
                        }}
                    alt={this.state.new.title} />
                </div>
                <div style={{display:'inline-block' }}>
                    <div style={{ fontWeight: 'bold'}}>{this.state.new.title}</div>
                    <div>{this.state.new.description}</div>
                </div>
            </div>
        )
    }
}