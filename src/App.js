import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import NewList from "./components/new-list";
import NewPage from "./components/new-page";

class App extends Component {

  render() {
    return (
      <Router>
        <div className="container">
          <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <Link to="/" className="navbar-brand">Home</Link>

          </nav>
          <br/>
          <Route path="/" exact component={NewList} />
          <Route path="/new/:id" component={NewPage} />
        </div>
      </Router>
    );
  }
}

export default App;
